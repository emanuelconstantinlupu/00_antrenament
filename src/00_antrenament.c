/*
 ============================================================================
 Name        : 00_antrenament.c
 Author      : Emanuel LUPU
 Version     :
 Copyright   : Restricted.
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int patrat(int a);
void comuta(int* a, int* b);

void functie()
{
	puts("=============[INCLUDES]============");
}

void aps()
{
	puts("-------------------------         \\");
	char str[80];
	gets(str);
	printf("Lungimea este %d", strlen(str));
	puts((char*) strlen(str));

	for (int i = 0; i < 10; i++)
	{
//		getche();
	}

}

int main(void)
{
	puts("============[BEGIN]=============");

	int a = 0;
	int b = 1;
	int c = 0;

	//~~~Afisare valori~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	printf("a = %d \n", a);
	printf("b = %d \n", b);

	//~~~Afisare dimensiuni~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	printf("Size of int: %d\n", sizeof(a));
	printf("Size of pint: %d\n", sizeof(&a));

	//~~~Afisare adrese~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	printf("Adresa lui a = %p\n", &a);
	printf("Adresa lui b = %p\n", &b);
	printf("Adresa lui c = %p\n", &c);

	comuta(&a, &b);
	printf("a = %d \n", a);
	printf("b = %d \n", b);

	printf("Adresa lui a = %p\n", &a);
	printf("Adresa lui b = %p\n", &b);
	printf("Adresa lui c = %p\n", &c);

	functie();

	puts("=============[END]=============");
	return EXIT_SUCCESS;
}

int patrat(int x)
{
	x = x * x;
	return (x);
}

void comuta(int *x, int *y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}
